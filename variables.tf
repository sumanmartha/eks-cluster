#
# Variables Configuration
#

variable "instance_type" {
	type = "string"
	default = "c5.large"
}

variable "key-pair" {
  type = "string"
  default = "test-key-prasad"
}

variable "eks_version" {
	type = "string"
	default = "1.13"
}

variable "cluster-name" {
  default = "eks-dev"
  type    = "string"
}

variable "aws_vpc_id" {
  type    = "string"
}

variable "aws_public_subnet_ids" {
  type    = "list"
}  

variable "aws_private_subnet_ids" {
  type    = "list"
}